import fs from 'fs';
import util from 'util';
import protobuf from 'protobufjs';

const readFile = util.promisify(fs.readFile);
const load = util.promisify(protobuf.load);

(async () => {
    const urlString = await readFile('url.txt', 'utf8');
    const url = new URL(urlString);
    const data = url.searchParams.get('data');
    const buffer = new Buffer(data, 'base64');
    
    const spec = await load('spec.proto');
    const format = spec.lookupType('MigrationPayload');
    const message = format.decode(buffer);

    console.log(message);
})();
